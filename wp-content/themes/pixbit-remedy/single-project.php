<?php get_header(); ?>

<?php
	$projectID = is_front_page() ? $GLOBALS['random_project_ID'] : $post->ID;
?>

<?php if(get_field('sections', $projectID)): ?>
	<div class="sections-wrapper">
  	<?php while(has_sub_field('sections', $projectID)): ?>
  		<?php get_template_part( 'parts/section', get_row_layout() ); // wp-content/themes/childtheme/slug-name.php ?> 
    <?php endwhile; ?>
	</div><!-- .sections-wrapper -->
<?php endif; ?>

<?php get_template_part( 'parts/projects', 'share' ); ?>
<?php get_template_part( 'parts/projects', 'next' ); ?>

<?php get_footer(); ?>