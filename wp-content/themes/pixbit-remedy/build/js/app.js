/*! Sidr - v1.2.1 - 2013-11-06
 * https://github.com/artberri/sidr
 * Copyright (c) 2013 Alberto Varela; Licensed MIT */
(function(e){var t=!1,i=!1,n={isUrl:function(e){var t=RegExp("^(https?:\\/\\/)?((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|((\\d{1,3}\\.){3}\\d{1,3}))(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*(\\?[;&a-z\\d%_.~+=-]*)?(\\#[-a-z\\d_]*)?$","i");return t.test(e)?!0:!1},loadContent:function(e,t){e.html(t)},addPrefix:function(e){var t=e.attr("id"),i=e.attr("class");"string"==typeof t&&""!==t&&e.attr("id",t.replace(/([A-Za-z0-9_.\-]+)/g,"sidr-id-$1")),"string"==typeof i&&""!==i&&"sidr-inner"!==i&&e.attr("class",i.replace(/([A-Za-z0-9_.\-]+)/g,"sidr-class-$1")),e.removeAttr("style")},execute:function(n,s,a){"function"==typeof s?(a=s,s="sidr"):s||(s="sidr");var r,d,l,c=e("#"+s),u=e(c.data("body")),f=e("html"),p=c.outerWidth(!0),g=c.data("speed"),h=c.data("side"),m=c.data("displace"),v=c.data("onOpen"),y=c.data("onClose"),x="sidr"===s?"sidr-open":"sidr-open "+s+"-open";if("open"===n||"toggle"===n&&!c.is(":visible")){if(c.is(":visible")||t)return;if(i!==!1)return o.close(i,function(){o.open(s)}),void 0;t=!0,"left"===h?(r={left:p+"px"},d={left:"0px"}):(r={right:p+"px"},d={right:"0px"}),u.is("body")&&(l=f.scrollTop(),f.css("overflow-x","hidden").scrollTop(l)),m?u.addClass("sidr-animating").css({width:u.width(),position:"absolute"}).animate(r,g,function(){e(this).addClass(x)}):setTimeout(function(){e(this).addClass(x)},g),c.css("display","block").animate(d,g,function(){t=!1,i=s,"function"==typeof a&&a(s),u.removeClass("sidr-animating")}),v()}else{if(!c.is(":visible")||t)return;t=!0,"left"===h?(r={left:0},d={left:"-"+p+"px"}):(r={right:0},d={right:"-"+p+"px"}),u.is("body")&&(l=f.scrollTop(),f.removeAttr("style").scrollTop(l)),u.addClass("sidr-animating").animate(r,g).removeClass(x),c.animate(d,g,function(){c.removeAttr("style").hide(),u.removeAttr("style"),e("html").removeAttr("style"),t=!1,i=!1,"function"==typeof a&&a(s),u.removeClass("sidr-animating")}),y()}}},o={open:function(e,t){n.execute("open",e,t)},close:function(e,t){n.execute("close",e,t)},toggle:function(e,t){n.execute("toggle",e,t)},toogle:function(e,t){n.execute("toggle",e,t)}};e.sidr=function(t){return o[t]?o[t].apply(this,Array.prototype.slice.call(arguments,1)):"function"!=typeof t&&"string"!=typeof t&&t?(e.error("Method "+t+" does not exist on jQuery.sidr"),void 0):o.toggle.apply(this,arguments)},e.fn.sidr=function(t){var i=e.extend({name:"sidr",speed:200,side:"left",source:null,renaming:!0,body:"body",displace:!0,onOpen:function(){},onClose:function(){}},t),s=i.name,a=e("#"+s);if(0===a.length&&(a=e("<div />").attr("id",s).appendTo(e("body"))),a.addClass("sidr").addClass(i.side).data({speed:i.speed,side:i.side,body:i.body,displace:i.displace,onOpen:i.onOpen,onClose:i.onClose}),"function"==typeof i.source){var r=i.source(s);n.loadContent(a,r)}else if("string"==typeof i.source&&n.isUrl(i.source))e.get(i.source,function(e){n.loadContent(a,e)});else if("string"==typeof i.source){var d="",l=i.source.split(",");if(e.each(l,function(t,i){d+='<div class="sidr-inner">'+e(i).html()+"</div>"}),i.renaming){var c=e("<div />").html(d);c.find("*").each(function(t,i){var o=e(i);n.addPrefix(o)}),d=c.html()}n.loadContent(a,d)}else null!==i.source&&e.error("Invalid Sidr Source");return this.each(function(){var t=e(this),i=t.data("sidr");i||(t.data("sidr",s),"ontouchstart"in document.documentElement?(t.bind("touchstart",function(e){e.originalEvent.touches[0],this.touched=e.timeStamp}),t.bind("touchend",function(e){var t=Math.abs(e.timeStamp-this.touched);200>t&&(e.preventDefault(),o.toggle(s))})):t.click(function(e){e.preventDefault(),o.toggle(s)}))})}})(jQuery);
// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

var is_header_visible = false;
var is_carousel_visible = false;
var is_video_visible = false;
var is_finished_loading = false;
var is_scrolled_to_top = false;
// var is_delay_over = false;

var top_bar_height = $('nav').outerHeight();
var carousel_height = $('.drop-down-carousel').height();

var left_menu_item_carrot = $('.left-menu-item .menu-item-stem');
var left_menu_item_text = $('#work-drop-down');
var right_menu_item_carrot = $('.right-menu-item .menu-item-stem');
var right_menu_item_text = $('#work-drop-down');

var menu_item_top = top_bar_height - left_menu_item_carrot.height();

//////////////////
// Hover Bounds //
//////////////////
var $w = $(window);
var viewportWidth = $w.width();
var viewportHeight = $w.height();

var boundSize = top_bar_height;

var leftZoneBound = boundSize;
var rightZoneBound = viewportWidth - boundSize;
var upperZoneBound = boundSize;
var lowerZoneBound = viewportHeight - boundSize;

var mousePos,
counter = 0;

$('.share-icons img').each(function(){
  var srcImg = $(this).attr('data-src');
  var hoverImg = $(this).attr('data-hover');
  //console.log(hoverImg);

  $(this).hover(function(){
    $(this).attr('src', hoverImg);
  }, function() {
    $(this).attr('src', srcImg);
  });
});

$('.form-icons img').each(function(){
  var srcImg = $(this).attr('data-src');
  var hoverImg = $(this).attr('data-hover');
  //console.log(hoverImg);

  $(this).hover(function(){
    $(this).attr('src', hoverImg);
  }, function() {
    $(this).attr('src', srcImg);
  });
});

$w.resize(function(){
  calculateVariables();
  centerHeaderContent();
  centerFooterContent();
  setup_tinyscrollbar();
  hideCarousel();
  combineAndPadLists();
  centerQuoteContent();
});

function combineAndPadLists(){
  var noTitleDiv = $('.list-block.no-list-title').first();
  var noTitleDivPadding = noTitleDiv.css('padding-top');

  var noTitleList = noTitleDiv.find('.no-list-title');
  var noTitleListPadding = noTitleList.css('padding-top');

  if($w.width() < 1028){
    noTitleDiv.css('padding-top', 0);
    noTitleList.css('padding-top', 0);
  }else{
    noTitleDiv.css('padding-top', 60);
    noTitleList.css('padding-top', 49);
  }
}

function calculateVariables(){
  top_bar_height = $('nav').outerHeight();
  carousel_height = $('.drop-down-carousel').height();

  left_menu_item_carrot = $('.left-menu-item .menu-item-stem');
  left_menu_item_text = $('#work-drop-down');
  right_menu_item_carrot = $('.right-menu-item .menu-item-stem');
  right_menu_item_text = $('#work-drop-down');

  menu_item_top = top_bar_height - left_menu_item_carrot.height();

  //////////////////
  // Hover Bounds //
  //////////////////
  $w = $(window);
  viewportWidth = $w.width();
  viewportHeight = $w.height();

  boundSize = top_bar_height;

  leftZoneBound = boundSize;
  rightZoneBound = viewportWidth - boundSize;
  upperZoneBound = boundSize;
  lowerZoneBound = viewportHeight - boundSize;
}

$(document).ready(function(){
  hideCarousel();
  centerHeaderContent();
  centerFooterContent();
  // centerMenuCarrots();
  combineAndPadLists();


  ///////////////////
  // Clicking Work //
  ///////////////////
  $('a#work-drop-down').click(function() {
    if(is_carousel_visible){
      hideCarousel();
    }else{
      showCarousel();
    }
    return false;
  });

  window.onmousemove = handleMouseMove;
  setInterval(getMousePosition, 100); // setInterval repeats every X ms

  // setInterval(function() {
  //     if(is_delay_over) return;  // do nothing!
  //     is_delay_over = true;
  // }, 10000); // every 5 sec

  ///////////////////
  // Header Clicks //
  ///////////////////
  $('#scroll-down').click(function() {
    scroll_to_next_section();
    is_scrolled_to_top = false;
  	// is_delay_over = true;
    return false;
  });

  $('#view-project').click(function() {
    scroll_to_next_section();
    is_scrolled_to_top = false;
  	// is_delay_over = true;
    return false;
  });

  if($('header').hasClass('header-section')){
    if($('#featuredvideoplayer').length > 0){
      var $iframe = $('#featuredvideoplayer')[0];
      var $player = $f($iframe);

      $('a#play-vimeo').click(function() {
        is_scrolled_to_top = false;
      	// is_delay_over = true;
        hideHeader();
        $('.header-content').hide();

        $('#featuredvideoplayer').show();
        is_video_visible = true;

        $player.api('play');

        return false;
      });

      $player.addEvent('ready', function() {
        // console.log('ready');

        $player.addEvent('pause', onPause);
        $player.addEvent('finish', onFinish);
        $player.addEvent('playProgress', onPlayProgress);
      });
    }
  }
});

///////////////////
// Images Loaded //
///////////////////
var image_count = 1;
var images = $('img');

$('.bg-img').each(function(){
  var el = $(this)
    , image = el.css('background-image').match(/url\((['"])?(.*?)\1\)/);
  if(image)
    images = images.add($('<img>').attr('src', image.pop()));
});

$('body').imagesLoaded()
  .always( function( instance ) {
		centerQuoteContent();
    // console.log('all images loaded');
  })
  .done( function( instance ) {
    // console.log('all images successfully loaded');
  })
  .fail( function() {
    // console.log('all images loaded, at least one is broken');
  })
  .progress( function( instance, image ) {
    var result = image.isLoaded ? 'loaded' : 'broken';
    // console.log( 'image is ' + result + ' for ' + image.img.src );
    if(image_count == 1){
			if($w.width() > 641) NProgress.start();
			setup_tinyscrollbar();
			showHeader();
		}else if(image_count == $('body img').length){
			if($w.width() > 641) NProgress.done();
			is_finished_loading = true;
		}else{
			if($w.width() > 641) NProgress.set( image_count/$('body img').length );
    }
    // console.log( image_count + '/' + $('body img').length );
    image_count++;
  });

/////////////////////
// Tiny Scroll Bar //
/////////////////////
function setup_tinyscrollbar(){
	var projects = $('.project-carousel .overview img');
	var carousel_row_width = $('.project-carousel .row').outerWidth();
	$w = $(window);
	var project_width = carousel_row_width / 8;
  if($w.width() < 641){
  	project_width = carousel_row_width / 4;
  }
	projects.each(function(){
		$(this).css('width', project_width * 3);
	});

	$('.overview').css('width', projects.first().outerWidth() * projects.length + 'px');
  console.log("projects.length: " + projects.length);
  console.log('overview width: ' + $('.overview').outerWidth());

	$('.scrollable').each(function(){
		var responsive_trackSize = 1128;

	  if($w.width() < 641){
			responsive_trackSize = $("photo-block-section").width();
		}

		var tiny_options = {
			axis: 'x',
			thumbSize: 70,
			trackSize: responsive_trackSize
		};

		$(this).tinyscrollbar(tiny_options);
	});
}

//////////////////////////
// Clear Gform on Click //
//////////////////////////
$(window).load(function() {
	$.fn.cleardefault = function() {
		return this.focus(function() {
			if( this.value == this.defaultValue ) {
				this.value = "";
			}
		}).blur(function() {
			if( !this.value.length ) {
				this.value = this.defaultValue;
			}
		});
	};
	$(".form-block-section input, .form-block-section textarea").cleardefault();
});

///////////////
// Hide Logo //
///////////////
$(window).scroll(function() {
  if ($(this).scrollTop() > 0) {
    hideHeader();
    is_scrolled_to_top = false;
  } else {
    showHeader();
    is_scrolled_to_top = true;
  }
});

////////////////////
// Header Toggles //
////////////////////
function showHeader(){
  $('nav, a.menu-item, #header-logo').css('opacity', '1');
  is_header_visible = true;
}

function hideHeader(){
	if(!is_scrolled_to_top){
  // if(is_delay_over){
	  $('nav, a.menu-item, #header-logo').css('opacity', '0');

	  if(is_carousel_visible){
	    hideCarousel();
	  }

	  is_header_visible = false;
	}
}

function showCarousel(){
  $('.drop-down-carousel').css('opacity', '1').css('top', top_bar_height);
  showLeftCarrot();
  hideRightCarrot();
  upperZoneBound = top_bar_height + carousel_height;
  is_carousel_visible = true;
}

function hideCarousel(){
  $('.drop-down-carousel').css('opacity', '0').css('top', '-10000px');
  hideLeftCarrot();
  showRightCarrot();
  upperZoneBound = top_bar_height;
  is_carousel_visible = false;
}

/////////////////////
// Mouse Detection //
/////////////////////
function handleMouseMove(event) {
  event = event || window.event; // IE-ism
  mousePos = {
    x: event.clientX,
    y: event.clientY
  };
}

function getMousePosition() {
  var pos = mousePos;
  if (!pos) { pos = {x: "?", y: "?"}; }

  if(pos.y < (upperZoneBound - 10)){
    showHeader();
  }else{
  	if($(window).scrollTop() !== 0) hideHeader();
  }
}


///////////////////////////////////////
// Header Content vertical alignment //
///////////////////////////////////////
function centerHeaderContent(){
	if($('header').hasClass('header-section')){
		var header_section_width = $(window).width();
		var header_content_width = $('.header-content').outerWidth();
		var scroll_button_width = $('#scroll-down').outerWidth();

		var header_content_left = (header_section_width - header_content_width) / 2;
		var scroll_button_left = (header_section_width - scroll_button_width) / 2;

		var header_section_height = $(window).height();
		var header_menu_height = $('.header-menu').outerHeight();
		var header_content_height = $('.header-content').outerHeight();

		var header_content_top = (header_section_height - header_content_height) / 2;

	  if($w.width() < 641){
      // console.log('$w.width() < 641');
			header_content_top = $('header nav').height() + $('#simple-menu-container').height() + 30;

      var mobile_height = header_content_height + header_content_top;
      // console.log('mobile_height: ' + mobile_height);

      if($('.long-text').length > 0){
    		$('#featuredvideoplayer').css('height', mobile_height);
    		$('.header-section').css('height', mobile_height);
      }else{
        $('#featuredvideoplayer').css('height', header_section_height);
        $('.header-section').css('height', header_section_height);
      }

      if($('.quote-rotator').length > 1){
        $('.quote-rotator').parent().removeClass('large-12').removeClass('columns');
      }
		}else{
  		$('#featuredvideoplayer').css('height', header_section_height);
  		$('.header-section').css('height', header_section_height);
    }


		$('.header-content').css('top', header_content_top);
		$('.header-content').css('left', header_content_left);
		$('#scroll-down').css('left', scroll_button_left);

		$('.header-content').css('opacity', '1');
	}
}

///////////////////////////////////////
// Footer Content vertical alignment //
///////////////////////////////////////
function centerFooterContent(){
	if($('.footer-section').length > 0){
		var footer_section_width = $(window).width();
		var footer_content_width = $('.footer-content').outerWidth();

		var footer_content_left = (footer_section_width - footer_content_width) / 2;

		var footer_section_height = $('.footer-section').height();
		var footer_content_height = $('.footer-content').outerHeight();

		var footer_content_top = (footer_section_height - footer_content_height) / 2;

		$('.footer-content').css('top', footer_content_top);
		$('.footer-content').css('left', footer_content_left);

		$('.footer-content').css('opacity', '1');
	}
}

///////////////////////////////
// Quote vertical alignment //
///////////////////////////////
function centerQuoteContent(){
  $('.photo-quote-section ul li.half-width-quote').each(function(){
    var quoteItem = $(this);

    if($w.width() < 640){
      quoteItem.appendTo(quoteItem.parent());
    }
  });

	$('.half-width-quote').each(function(){
	  var quoteBlock = $(this);
	  var quotePadding = (($(quoteBlock).parent().height() - $(quoteBlock).height()) / 2);

    if($w.width() < 640){
      quoteBlock.css('padding-top', 0);
    }else{
      quoteBlock.css('padding-top', quotePadding);
    }

	  // console.log($(quoteBlock).parent().height());
	  // console.log($(quoteBlock).height());
	  // console.log(quoteBlock[0]);
	});
}

///////////////////////////////////
// Menu Carrots center alignment //
///////////////////////////////////
function centerMenuCarrots(){
	centerLeftCarrot();
	centerRightCarrot();
}

function centerLeftCarrot(){
	var menu_item_padding = parseInt($('.columns').css('padding-left').replace(/[^-\d\.]/g, ''));
	var left_menu_item_padding = menu_item_padding + ((left_menu_item_text.outerWidth() - left_menu_item_carrot.outerWidth()) / 2);

	left_menu_item_carrot.css('left', left_menu_item_padding);
}

function hideLeftCarrot(){
	left_menu_item_carrot.css('opacity', '0').css('top', '-10000px');
}

function showLeftCarrot(){
	left_menu_item_carrot.css('opacity', '1').css('top', menu_item_top);
}

function centerRightCarrot(){
	var menu_item_padding = parseInt($('.columns').css('padding-right').replace(/[^-\d\.]/g, ''));
	var right_menu_item_padding = menu_item_padding + ((right_menu_item_text.outerWidth() - right_menu_item_carrot.outerWidth()) / 2);
	right_menu_item_carrot.css('right', right_menu_item_padding);

	showRightCarrot();
}

function hideRightCarrot(){
	if($('article').hasClass('is-info-page')){
		right_menu_item_carrot.css('opacity', '0').css('top', '-10000px');
	}
}

function showRightCarrot(){
	if($('article').hasClass('is-info-page')){
		right_menu_item_carrot.css('opacity', '1').css('top', menu_item_top);
	}
}

///////////////
// Vimeo API //
///////////////
if($('header').hasClass('header-section')){
	$(window).load(function() {
		// cached objects
		var $b = $('body');
		var $d = $(document);
		var $w = $(window);
		var $iframe = $('#featuredvideoplayer')[0];

		if($('#featuredvideoplayer').length > 0){
			var $player = $f($iframe);

			// static vars
		  var video_advance_cuepoint = 5;
		  var text_scrolled = false;

			// When the player is ready, add listeners for pause, finish, and playProgress
			$player.addEvent('ready', function() {
		    // console.log('VIMEO READY');
				$('#play-vimeo').css('opacity', '1');

		    $player.addEvent('pause', onPause);
		    $player.addEvent('finish', onFinish);
		    $player.addEvent('playProgress', onPlayProgress);
			});

			// check play progress
		  // if (chapter != 'title' && !is_touch) {
		      $player.addEvent('playProgress', function() {
		          // skip ahead to the explainer text at a particular cuepoint
		          $player.api('getCurrentTime', function(time) {
		              $player.api('getDuration', function(duration) {
											// console.log('not done:' + time + '/' + duration);
		                  if ((duration - time <= video_advance_cuepoint) && (duration > 0) && text_scrolled == false ) {
												// console.log('DONE:' + time + '/' + duration);
		                    // scroll_to_next_section();
		                    // text_scrolled = true;
		                  }
		              });
		          });
		      });
		  // }
		}

	  function scroll_to_top(){
		  $w.scrollTop(0);
		}

	  function on_scroll() {
	      var scroll_position = $d.scrollTop();
	      // if (scroll_position >= nav_height) {
	      //     $b.addClass('scrolling').removeClass('scrolling-off');
	      // } else {
	      //     $b.addClass('scrolling-off').removeClass('scrolling');
	      // }
	  }

		// $player.api('play');
		// $player.api('pause');
		// $player.api('seekTo', 60);
		// $player.api('getVideoHeight', function (value, player_id) {
		//   console.log('getVideoHeight : ' + value);
		// });
		// $player.api('getDuration', function (value, player_id) {
		//   console.log('getDuration : ' + value);
		// });
		// $player.api('getVideoUrl', function (value, player_id) {
		//   console.log('getVideoUrl : ' + value);
		// });
		// $player.api('getColor', function (value, player_id) {
		//   console.log('getColor : ' + value);
		// });
		// $player.api('setColor', 'ffffff');


		// Call the API when a button is pressed
		// $('button').bind('click', function() {
	 //    $player.api($(this).text().toLowerCase());
		// });

		function onPause(id) {
	    // console.log('paused');
		}

		function onFinish(id) {
	    // console.log('finished');
	    scroll_to_next_section();
		}

		function onPlayProgress(data, id) {
	    // console.log(data.seconds + 's played');
		}
	});
}

/*
 * Explainer text
 */
function scroll_to_next_section() {
  $.smoothScroll({
    	speed: 1500,
      offset: $(window).height()
	});
}
