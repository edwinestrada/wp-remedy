<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Page description goes here">

<title><?php bloginfo('name'); ?></title>

<link href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" rel="stylesheet">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link href="//cloud.webtype.com/css/5604fd6e-7017-4bec-ab5f-0faa4413a27a.css" rel="stylesheet" type="text/css" />

<?php wp_head(); ?>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-50502313-1', 'workbyremedy.co');
	ga('send', 'pageview');
</script>
</head>
<?php
	$body_class = '';
	if(is_front_page()){
		$body_class .= ' is-front-page';
	}
?>
<body class="<?php echo $body_class; ?>">
<div class="drop-down-carousel">
	<?php get_template_part( 'parts/projects', 'carousel' ); ?>
</div>
<?php
	$qry_args = array(
		'post_status' => 'publish', // optional
		'post_type' => 'project', // Change to match your post_type
		'posts_per_page' => -1, // ALL posts
		'category_name' => 'featured'
	);

	$all_posts = new WP_Query( $qry_args );
	$projectIDs = array();

	foreach ($all_posts->posts as $this_post) {
		array_push($projectIDs, $this_post->ID);
	}

	$random_KEY = array_rand($projectIDs);
	$GLOBALS['random_project_ID'] = $projectIDs[$random_KEY];
?>
<?php
if(is_front_page()){
	$header_type = get_field('header_type', $GLOBALS['random_project_ID']);
	$header_image = get_field('header_image', $GLOBALS['random_project_ID']);
	$header_video_id = get_field('header_video_id', $GLOBALS['random_project_ID']);
	$header_text = get_field('header_text', $GLOBALS['random_project_ID']);
	$title = get_the_title($GLOBALS['random_project_ID']);
}else{
	$header_type = get_field('header_type');
	$header_image = get_field('header_image');
	$header_video_id = get_field('header_video_id');
	$header_text = get_field('header_text');
	$title = get_the_title($post->ID);
}
$remedy_logo = get_field('remedy_logo', 'options');
?>
<div id="simple-menu-container" class="sticky hide-for-large-up">
	<a id="simple-menu">
		<i class="fa fa-angle-left"></i>
	</a>
</div>

<div id="sidr">
  <ul>
    <li class="active"><a href="<?php bloginfo ('url'); ?>">Home</a></li>
    <li><a href="#" style="line-height:10px;">—</a></li>
    <li><a href="<?php bloginfo ('url'); ?>/work">Work</a></li>
    <li><a href="<?php bloginfo ('url'); ?>/info">Info</a></li>
  </ul>
</div>

<script>
$(document).ready(function() {
  $('#simple-menu').sidr({
		onOpen: function(){
			console.log('onOpen');
			$('nav, a.menu-item, #header-logo').remove();
			// $('#simple-menu-container').removeClass('sticky')
			$('#simple-menu-container').css('left', $('#sidr').width());

			$('#simple-menu i').addClass('fa-angle-right');
			$('#simple-menu i').removeClass('fa-angle-left');
		},
		onClose: function(){
			console.log('onClose');
			// $('#simple-menu-container').addClass('sticky');
			$('#simple-menu-container').css('left', '0');

			$('#simple-menu i').removeClass('fa-angle-right');
			$('#simple-menu i').addClass('fa-angle-left');
		},
		displace: false
	});


});
</script>

<?php if($header_image): ?>
<header class="full-width-bg header-section post-<?php echo $post->ID; ?> bg-img" style="background-image: url(<?php echo $header_image['url']; ?>);">
<?php else: ?>
<header>
<?php endif; ?>
<div class="sticky">
	<nav>
	<div class="row header-menu">
		<div class="large-3 hide-for-medium-only hide-for-small-only columns">
			<div class="menu-item left-menu-item">
				<a id="work-drop-down" href="#work">Work</a>
			</div>
		</div>
		<div class="large-6 medium-12 small-12 columns">
			<a id="logo-link" href="<?php bloginfo ('url'); ?>" class="large-centered columns">
				<img id="header-logo" src="<?php echo $remedy_logo['url']; ?>" />
			</a>
		</div>
		<div class="large-3 hide-for-medium-only hide-for-small-only columns">
			<div class="menu-item right-menu-item">
				<a href="<?php bloginfo ('url'); ?>/info">Info</a>
				<!-- <span class="menu-item-stem"></span> -->
			</div>
		</div>
	</div>
	</nav>
</div>

<?php
	switch ($header_type) {
		case 'vimeo_video':
		////////////////////////////////////////////
		// Header title & Watch the Film button //
		////////////////////////////////////////////
		?>
		<iframe id="featuredvideoplayer" src="//player.vimeo.com/video/<?php echo $header_video_id; ?>?portrait=0&amp;color=e68825&amp;player_id=featuredvideoplayer&amp;api=1" width="100%" height="90%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		<div class="header-content">
			<h1><?php echo $title; ?></h1>
			<a id="play-vimeo" class="banner-link" href="#play">Watch the Film</a>
		</div>
		<?php
		break;

		case 'view_project':
		//////////////////////////////////////////
		// Header title & View Project button //
		//////////////////////////////////////////
		?>
		<div class="header-content">
			<h1><?php echo $title; ?></h1>
			<a id="view-project" href="#scroll">View Project</a>
		</div>
		<?php
		break;

		case 'arrow_down':
		////////////////////////
		// Scroll down arrow //
		////////////////////////
		?>
		<div class="header-content">
			<div class="row">
				<div class="text-header large-12 columns">
					<p class="long-text"><?php echo $header_text; ?></p>
				</div>
			</div><!-- .row -->
		</div>
		<a id="scroll-down" href="#scroll"></a>
		<?php
		break;

		default:
			break;
	}
?>
</header>
<article id="page-<?php the_ID(); ?>" <?php post_class(is_page_template('content-info.php') ? 'is-info-page' : 'is-not-info-page'); ?>>
