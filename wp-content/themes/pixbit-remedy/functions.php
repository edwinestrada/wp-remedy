<?php
/**
 * Pixbit Berry Child Theme Functions
 */

add_filter('show_admin_bar', '__return_false');

//////////////
// 0. DEBUG //
//////////////

// add_action( 'wp_print_styles', 'print_queue_scripts' );
function print_queue_scripts(){
  global $wp_scripts;

  echo "<br/>";
  echo "SCRIPTS:";
  echo "<br/>";
  foreach( $wp_scripts->queue as $handle ) :
    echo $handle . ' | ';
  endforeach;
}

////////////////////////////
// Advanced Custom Fields //
////////////////////////////

/**
 * Sets up Custom Post Types to be used by ACF
 */
function acf_options_page_settings($settings){
  $settings['title'] = 'Options';
  $settings['pages'] = array(
    'Sharing'
    );

  if(!class_exists('CPT'))
  require_once(get_template_directory().'/inc/cpt-lib/CPT.php');

  /**
   * @var CPT(PLURAL_NAME, SINGULAR_NAME, SLUG)
   */
  $Projects = new CPT('Projects', 'Project', 'project');
  $Projects->setIcon('redPin');
  $Projects->setTaxonomies(array('category'));
  $Projects->load();

  add_theme_support('post-thumbnails');

  return $settings;
}
add_filter('acf/options_page/settings', 'acf_options_page_settings');

/////////////////////////////////////////////////
// Enqueue Scripts and Styles for Front-End //
/////////////////////////////////////////////////
function get_theme_assets() {
  /////////////////////////////////
  // Load jQuery from Google CDN //
  /////////////////////////////////
  wp_deregister_script( 'jquery' ); // Deregister the included library
  wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', array(), null, false );
  wp_enqueue_script('jquery');

  //////////////////
  // Google Fonts //
  //////////////////
  // wp_enqueue_style('googlefonts-Oswald', 'http://fonts.googleapis.com/css?family=Oswald:400,300,700');

  //////////////////
  // NProgress    //
  //////////////////
  wp_register_script('nprogress_js', get_stylesheet_directory_uri().'/js/vendor/nprogress.js', array(), false, true);
  wp_enqueue_script('nprogress_js');

  //////////////////
  // ImagesLoaded //
  //////////////////
  wp_register_script('imagesloaded_js', get_stylesheet_directory_uri().'/js/vendor/imagesloaded.pkgd.min.js', array(), false, true);
  wp_enqueue_script('imagesloaded_js');

  ///////////////
  // Modernizr //
  ///////////////
  wp_register_script('modernizr_js', get_stylesheet_directory_uri().'/js/vendor/modernizr.js', array(), false, true);
  wp_enqueue_script('modernizr_js');

  //////////////////////////
  // Zurb Foundation 5 JS //
  //////////////////////////
  wp_register_script('foundation_js', get_stylesheet_directory_uri().'/js/foundation.min.js', array(), false, true);
  wp_enqueue_script('foundation_js');

  //////////////////////////
  // Vimeo Froogaloop API //
  //////////////////////////
  wp_enqueue_script(
    'vimeo-froogaloop'
    , '//a.vimeocdn.com/js/froogaloop2.min.js'
    , array('jquery')
  );

  ///////////////////
  // Smooth Scroll //
  ///////////////////
  wp_enqueue_script(
    'jquery-smooth-scroll'
    , get_stylesheet_directory_uri() . '/js/jquery.smooth-scroll.min.js'
    , array('jquery')
  );

  ///////////////////
  // Tinyscrollbar //
  ///////////////////
  wp_enqueue_script(
    'jquery-tinyscrollbar'
    , get_stylesheet_directory_uri() . '/js/jquery.tinyscrollbar.js'
    , array('jquery')
  );

  /////////////////////////////////////
  // Google JS-API for Adobe Typekit //
  /////////////////////////////////////
  // wp_register_script( 'jsapi', '//www.google.com/jsapi', array(), null, false );
  // wp_enqueue_script('jsapi');

  //////////////
  // Theme JS //
  //////////////
  wp_register_script('remedy_js', get_stylesheet_directory_uri().'/build/js/app.js', array(), false, true);
  wp_enqueue_script('remedy_js');

  ///////////////////
  // Quote Rotator //
  ///////////////////
  wp_enqueue_script(
    'simple_text_rotator_js'
    , get_stylesheet_directory_uri() . '/js/vendor/jquery.simple-text-rotator.min.js'
    , array('jquery')
  );

}
add_action( 'wp_enqueue_scripts', 'get_theme_assets' );

////////////////////
// 1. Admin Menus //
////////////////////

/**
 * Hides the text/visual content editor.
 * Can base it on Post ID, User roles, or template name.
 */
function hide_editor() {
  // Get the Post ID.
  // $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  // if( !isset( $post_id ) ) return;

  // Get the name of the Page Template file.
  // $template_file = get_post_meta($post_id, '_wp_page_template', true);
  // if($template_file == 'contact.php'){ // edit the template name
  //   remove_post_type_support('page', 'editor');
  // }

  remove_post_type_support('page', 'editor');
}
// add_action( 'admin_init', 'hide_editor' );

/////////////////////////////////
// 2. Register Nav Menu //
/////////////////////////////////

function register_my_menus() {
  register_nav_menus(
    array(
      'main-menu' => 'Main Menu'
      ,'footer-menu' => 'Footer Menu'
    )
  );
}
// add_action( 'init', 'register_my_menus' );

/////////////////////////
// Strip and lowercase //
/////////////////////////

if ( ! function_exists( 'filter_string' ) ) :
function filter_string($str){
  $str = preg_replace("/\s+/", " ", $str);
  $str = str_replace(" ", "", $str); // Spaces
  $str = preg_replace("/[^A-Za-z0-9_-]/","",$str);
  $str = strtolower($str);

  return $str;
}
endif;
