</article>
<footer>
	<div class="row">
		<ul>
			<li class='left-footnote large-4 small-6 columns'>&copy; 2014 Remedy Co. LLC</li>
			<li class='left-footnote large-4 columns hide-for-small-only hide-for-medium-only'><a style="color:black;" href="https://plus.google.com/111530648532375595942" rel="publisher">G+</a></li>
			<li class='right-footnote large-4 small-6 columns'><a href="http://www.thinkpixbit.com">Site by Pixbit</a></li>
		</ul>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
