<?php
/*
 * Template Name: Info Page
 */
get_header();
?>
<?php if(get_field('sections')): ?>
	<div class="sections-wrapper">
		<?php $section_counter = 0; ?>
  	<?php
  	while(has_sub_field('sections')):
  		include(locate_template('parts/section-'.get_row_layout().'.php'));
  		$section_counter++;
    endwhile;
    ?>
	</div><!-- .sections-wrapper -->
<?php endif; ?>

<div class='form-block-section row'>
	<div class="large-6 columns">
		<h3>Inquiries</h3>
		<ul class="contact-texts">
			<li><span>Talk</span>919-473-6613</li>
			<li><span>Type</span>hello@workbyremedy.co</li>
		</ul>

		<h3 class="form-icons-header">Elsewhere</h3>
		<?php
			$social_icons = array();

			while(have_rows('social_icons', 'options')): the_row();
				array_push($social_icons, get_sub_field('icon_block', 'options'));
			endwhile;
		?>

		<?php if(count($social_icons) > 0): ?>
			<ul class='form-icons small-block-grid-4 large-12 small-12'>
				<?php $i = 0; ?>
				<?php while(have_rows('social_icons', 'options')): the_row(); ?>
					<?php $icon_block = get_sub_field('icon_block'); ?>
					<?php $icon_hover = get_sub_field('icon_hover'); ?>
					<?php $icon_link = get_sub_field('icon_link'); ?>

					<li class='form-icon'>
						<a class="icon-<?php echo $i; ?>" href="<?php echo $icon_link; ?>" target="_blank">
							<img data-hover="<?php echo $icon_hover['url']; ?>" data-src="<?php echo $icon_block['url']; ?>" src="<?php echo $icon_block['url']; ?>">
						</a>
					</li><!-- .form-icon -->


				  <?php $i++; ?>
				<?php endwhile; ?>
			</ul><!-- .form-icons -->
		<?php endif; ?>
	</div>
	<div class="large-6 columns">
		<h3>Sign up for our newsletter</h3>
		<?php
		  $form = get_field('form');
		  gravity_form_enqueue_scripts($form->id, true);
		  gravity_form($form->id, false, false, false, '', true, 1);
		?>
	</div>
</div>

<?php get_footer(); ?>
