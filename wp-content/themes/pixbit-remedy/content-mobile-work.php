<?php
// Template Name: Mobile – Work
get_header(); ?>
<?php
$loop = new WP_Query(
  array(
  'post_type'      => 'project',
  'posts_per_page' => '-1',
  'orderby'        => 'menu_order date',
  'order'          => 'DESC'
));

$i = 0;
$projects = array();
while($loop->have_posts()) : $loop->the_post();
/* Post */
$this[$i]['ID']        		 = $post->ID;
$this[$i]['title']         = get_the_title($post->ID);
$this[$i]['link']          = get_permalink($post->ID);
$this[$i]['modified_time'] = get_the_modified_time('F j, Y');
$this[$i]['excerpt']       = get_the_excerpt();
/***********************************************************************************************************************/

/* Featured Image */
if ( has_post_thumbnail($post->ID) ) {
  $this[$i]['imageON'] = TRUE;
  $this[$i]['imageURL'] = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
  $this[$i]['image']       = wp_get_attachment_image(get_post_thumbnail_id($post->ID));
}else{
  $this[$i]['imageON'] = FALSE;
}
/***********************************************************************************************************************/

array_push($projects, $this[$i]);
$i++;
endwhile;
wp_reset_postdata(); // reset the query
?>
<div class="work-list">
      <?php foreach( $projects as $project ): ?>
        <?php if($project['imageON']): ?>
          <a href="<?php echo get_permalink($project['ID']); ?>"><img class="with-gutters" src="<?php echo $project['imageURL']; ?>" /></a>
        <?php endif; ?>
      <?php endforeach; //foreach project ?>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $('.header-content').remove();
  $('nav, a.menu-item, #header-logo').remove();
});
</script>

<?php get_footer(); ?>
