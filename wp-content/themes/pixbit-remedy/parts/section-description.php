<?php
/**
 * ACF Section - Description Section
 */
?>
<div class="section-description">
	<div class="row">
		<div class="large-offset-1 large-8 large-push-3 columns description-block">
			<h1><?php echo get_sub_field('title'); ?></h1>
			<p class="subtitle"><?php echo get_sub_field('subtitle'); ?></p>
			<h2 class="uppercase-caption"><?php echo get_sub_field('uppercase_caption'); ?></h2>
			<p class="caption"><?php echo get_sub_field('caption'); ?></p>
		</div>
		<div class="large-3 large-pull-9 columns list-block <?php echo get_sub_field('list_title') ? 'has-list-title' : 'no-list-title'; ?>">
			<h2><?php echo get_sub_field('list_title'); ?></h2>
			<p>&ndash;</p>
			<?php if(have_rows('list_items')): ?>
				<ul>
				<?php while(have_rows('list_items')): the_row(); ?>
					<li><span><?php echo get_sub_field('title'); ?></span> <?php echo get_sub_field('name'); ?></li>
				<?php endwhile; // $list_items ?>
				</ul>
			<?php endif; // $list_items ?>
		</div>
	</div>
</div><!-- .info-section -->