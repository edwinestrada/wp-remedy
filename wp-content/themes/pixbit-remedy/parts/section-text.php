<?php
/**
 * ACF Section - Text Section
 */
?>
<?php
	$is_full_width = get_sub_field('is_full_width');
	$text_image = get_sub_field('text_image');
	$gutter_locations = get_sub_field('gutter_locations');
?>
<?php if($text_image): ?>
<style type="text/css">
	#text-section-<?php echo $section_counter; ?>{
		background-image: url(<?php echo $text_image['url']; ?>);
	  background-position: center top;
	  -webkit-background-size: cover;
	  -moz-background-size: cover;
	  -o-background-size: cover;
	  background-size: cover;
	  position: relative;
	}
	#text-section-<?php echo $section_counter; ?> .row{
		padding-left: 0;
		padding-right: 0;
		background: none;
	}
</style>
<?php endif; ?>
<div id="text-section-<?php echo $section_counter; ?>" class="text-section <?php echo $is_full_width ? 'full-text-section' : ''; ?> bg-img <?php echo $gutter_locations; ?>">
	<div class="row">
		<div class="large-12 columns">

			<?php if( get_sub_field('long_text') ): ?>
				<p class="long-text"><?php echo get_sub_field('long_text'); ?></p>
			<?php endif; ?>

			<?php if( get_sub_field('text_name') ): ?>
				<?php if( get_sub_field('long_text') ): ?>
					<div class="hr"><hr /></div>
				<?php endif; ?>
				<h2><?php echo get_sub_field('text_name'); ?></h2>
			<?php endif; ?>

		</div>
	</div>
<?php if(get_sub_field('enable_carrot')): ?>
	<span class="text-stem"></span>
<?php endif; ?>
</div>