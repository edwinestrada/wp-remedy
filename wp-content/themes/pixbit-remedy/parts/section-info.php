<?php
/**
 * ACF Section - Info Section
 */
?>
<div class="section-info info-section">
	<div class="row">

		<?php if(have_rows('paragraphs')): ?>
			<?php while(have_rows('paragraphs')): the_row(); ?>
				<div class="large-6 columns list-block">
					<h2><?php echo get_sub_field('paragraph_title'); ?></h2>
					<p><?php echo get_sub_field('paragraph_text'); ?></p>
				</div>
			<?php endwhile; // $paragraphs ?>
		<?php endif; // $paragraphs ?>

		<?php if(have_rows('lists')): ?>
			<?php while(have_rows('lists')): the_row(); ?>
				<div class="large-2 large-offset-1 columns list-block <?php echo get_sub_field('list_title') ? 'has-list-title' : 'no-list-title'; ?>">
					<?php if(get_sub_field('list_title')): ?>
						<h2><?php echo get_sub_field('list_title'); ?></h2>
					<?php endif; ?>
						<ul class="<?php echo get_sub_field('list_title') ? 'has-list-title' : 'no-list-title'; ?>">
					<?php if(have_rows('list_items')): ?>
						<?php while(have_rows('list_items')): the_row(); ?>
							<li><span><?php echo get_sub_field('text'); ?></span></li>
						<?php endwhile; ?>
					<?php endif; // $list_items ?>
						</ul>
				</div>
			<?php endwhile; // $lists ?>
		<?php endif; // $lists ?>

	</div>
</div><!-- .info-section -->