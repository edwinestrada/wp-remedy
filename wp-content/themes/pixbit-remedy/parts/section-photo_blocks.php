<?php
/**
 * ACF Section - Photo Blocks Section
 *
 * Can be 2 or 3 pictures.
 */
?>
<?php
	$images = array();
	$gutter_locations = get_sub_field('gutter_locations');

	while(have_rows('image_blocks')): the_row();
		array_push($images, get_sub_field('image_block'));
	endwhile;
?>

<?php if(count($images) > 0): ?>
	<div class='photo-block-section row'>
		<ul class="large-block-grid-<?php echo count($images); ?> <?php echo $gutter_locations; ?>">
			<?php while(have_rows('image_blocks')): the_row(); ?>
				<?php $image_block = get_sub_field('image_block'); ?>
			  <li><img class="<?php echo $gutter_locations; ?>" src="<?php echo $image_block['url']; ?>" /></li>
			<?php endwhile; ?>
		</ul>
	</div>
<?php endif; ?>