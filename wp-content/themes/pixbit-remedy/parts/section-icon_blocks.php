<?php
/**
 * ACF Section - Icon Blocks Section
 *
 * Stacks 3 icons per row.
 */
?>
<?php
	$icons = array();
	$header = get_sub_field('header');

	while(have_rows('icons')): the_row();
		array_push($icons, get_sub_field('icon_block'));
	endwhile;
?>

<?php if(count($icons) > 0): ?>
	<div class='icons-block-section row'>
		<h2 class="large-centered columns">Select Clients</h2>
		<ul class="large-block-grid-3 <?php echo $header; ?>">
			<?php while(have_rows('icons')): the_row(); ?>
				<?php $icon_block = get_sub_field('icon_block'); ?>
				<?php $icon_hover = get_sub_field('icon_hover'); ?>
				<?php $icon_link = get_sub_field('icon_link'); ?>
			  <li>
			  	<a href="<?php echo $icon_link; ?>" target="_blank">
				  	<img class="<?php echo $header; ?> icon-block" src="<?php echo $icon_block['url']; ?>" />
				  	<img class="<?php echo $header; ?> icon-hover" src="<?php echo $icon_hover['url']; ?>" />
			  	</a>
			  </li>
			<?php endwhile; ?>
		</ul>
	</div>
<?php endif; ?>