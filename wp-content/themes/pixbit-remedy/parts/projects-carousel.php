<?php
$loop = new WP_Query(
  array(
  'post_type'      => 'project',
  'posts_per_page' => '-1',
  'orderby'        => 'menu_order date',
  'order'          => 'DESC'
));

$i = 0;
$projects = array();
while($loop->have_posts()) : $loop->the_post();
/* Post */
$this[$i]['ID']            = $post->ID;
$this[$i]['title']         = get_the_title($post->ID);
$this[$i]['link']          = get_permalink($post->ID);
$this[$i]['modified_time'] = get_the_modified_time('F j, Y');
$this[$i]['excerpt']       = get_the_excerpt();
/***********************************************************************************************************************/

/* Featured Image */
if ( has_post_thumbnail($post->ID) ) {
  $this[$i]['imageON'] = TRUE;
  $this[$i]['imageURL'] = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
  $this[$i]['image']       = wp_get_attachment_image(get_post_thumbnail_id($post->ID));
}else{
  $this[$i]['imageON'] = FALSE;
}
/***********************************************************************************************************************/

array_push($projects, $this[$i]);
$i++;
endwhile;
wp_reset_postdata(); // reset the query
?>
<?php if(count($projects) < 4): ?>
  <div class='project-carousel'>
    <div class='photo-block-section row'>
      <ul class='large-block-grid-3 with-gutters'>
        <?php foreach( $projects as $project ): ?>
          <?php if($project['imageON']): ?>
            <li><a href="<?php echo get_permalink($project['ID']); ?>"><img class='with-gutters' src="<?php echo $project['imageURL']; ?>" alt="<?php echo $project['title']; ?>"></a></li>
          <?php endif; ?>
        <?php endforeach; //foreach project ?>
      </ul>
    </div>
  </div>
<?php else: ?>
  <div class='project-carousel'>
    <div class='photo-block-section row'>
      <div class='scrollable'>
        <div class='scrollbar'><div class='track'><div class='thumb'><div class='end'></div></div></div></div>
        <div class='viewport'>
          <div class='overview'>
            <?php foreach( $projects as $project ): ?>
              <?php if($project['imageON']): ?>
                <a href="<?php echo get_permalink($project['ID']); ?>"><img src="<?php echo $project['imageURL']; ?>" alt="<?php echo $project['title']; ?>"></a>
              <?php endif; ?>
            <?php endforeach; //foreach project ?>
          </div><!--overview-->
        </div><!--viewport-->
      </div><!-- .scrollable -->
    </div>
  </div><!-- project-carousel -->
<?php endif; ?>
