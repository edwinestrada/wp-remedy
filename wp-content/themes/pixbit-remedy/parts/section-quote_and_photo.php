<?php
/**
 * ACF Section - Quote and Photo Section
 */
?>
<?php $quote_layout = get_sub_field('type'); ?>
<?php $quote_image = get_sub_field('quote_image'); ?>
<?php switch ($quote_layout) {
	case 'quote-photo': ?>
	<div class="quote-photo-section row">
		<ul class="large-block-grid-2">
			<li><img class="no-gutters" src="<?php echo $quote_image['url']; ?>" /></li>
			<li class="half-width-quote">
				<div class="row">
					<div class="large-offset-2 large-8 columns quote-text">

						<?php if( get_sub_field('short_quote') ): ?>
							<p class="quote columns"><?php echo get_sub_field('short_quote'); ?></p>
						<?php endif; ?>

						<?php if( get_sub_field('long_quote') ): ?>
							<?php if( get_sub_field('short_quote') ): ?>
								<p class="separator columns">&ndash;</p>
							<?php endif; ?>
							<p class="long-quote"><?php echo get_sub_field('long_quote'); ?></p>
						<?php endif; ?>

						<?php if( get_sub_field('quote_name') ): ?>
							<?php if( get_sub_field('short_quote') || get_sub_field('long_quote') ): ?>
								<p class="separator columns">&ndash;</p>
							<?php endif; ?>
							<h2 class="columns"><?php echo get_sub_field('quote_name'); ?></h2>
						<?php endif; ?>

					</div><!-- .large-offset-2 .large-8 .columns -->
				</div><!-- .row -->
			</li><!-- .half-width-quote -->
		</ul>
	</div>

	<?php break;

	case 'photo-quote': ?>

	<div class="photo-quote-section row">
		<ul class="large-block-grid-2">
			<li class="half-width-quote">
				<div class="row">
					<div class="large-offset-2 large-8 columns quote-text">

						<?php if( get_sub_field('short_quote') ): ?>
							<p class="quote columns"><?php echo get_sub_field('short_quote'); ?></p>
						<?php endif; ?>

						<?php if( get_sub_field('long_quote') ): ?>
							<?php if( get_sub_field('short_quote') ): ?>
								<p class="separator columns">&ndash;</p>
							<?php endif; ?>
							<p class="long-quote columns"><?php echo get_sub_field('long_quote'); ?></p>
						<?php endif; ?>

						<?php if( get_sub_field('quote_name') ): ?>
							<?php if( get_sub_field('short_quote') || get_sub_field('long_quote') ): ?>
								<p class="separator columns">&ndash;</p>
							<?php endif; ?>
							<h2 class="columns"><?php echo get_sub_field('quote_name'); ?></h2>
						<?php endif; ?>

					</div><!-- .large-offset-2 .large-8 .columns -->
				</div><!-- .row -->
			</li><!-- .half-width-quote -->
			<li><img class="no-gutters" src="<?php echo $quote_image['url']; ?>" /></li>
		</ul>
	</div>

	<?php break;

	case 'quote': ?>

	<div class="quote-section row">
		<div class="large-offset-1 large-10 columns">
			<p class="separator">&ndash;</p>
			<?php if( get_sub_field('short_quote') ): ?>
				<p class="quote"><?php echo get_sub_field('short_quote'); ?></p>
			<?php endif; ?>

			<?php if( get_sub_field('long_quote') ): ?>
				<?php if( get_sub_field('short_quote') ): ?>
					<p class="separator">&ndash;</p>
				<?php endif; ?>
				<p class="long-quote"><?php echo get_sub_field('long_quote'); ?></p>
			<?php endif; ?>

			<?php if( get_sub_field('quote_name') ): ?>
				<?php if( get_sub_field('short_quote') || get_sub_field('long_quote') ): ?>
					<p class="separator">&ndash;</p>
				<?php endif; ?>
				<h2><?php echo get_sub_field('quote_name'); ?></h2>
			<?php endif; ?>
			<p class="separator">&ndash;</p>

		</div>
	</div>

	<?php break;

	default:
		# code...
		break;
}?>
