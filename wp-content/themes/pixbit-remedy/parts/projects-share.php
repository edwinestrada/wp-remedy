<?php
/**
 * ACF Section - Share Blocks Section
 *
 */
?>

<div class='share-block-section row'>
	<h2 class="large-centered columns">Share this project</h2>
		<?php
			if(is_front_page()){
				$permalink = get_permalink($GLOBALS['random_project_ID']);
				$image = wp_get_attachment_url( get_post_thumbnail_id($GLOBALS['random_project_ID']));
			}else{
				$permalink = get_permalink($post->ID);
				$image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
			}
			$url = urlencode($permalink);
		?>

		<?php
			$icons = array();

			while(have_rows('icons', 'options')): the_row();
				array_push($icons, get_sub_field('icon_block', 'options'));
			endwhile;
		?>

		<?php if(count($icons) > 0): ?>
			<ul class='share-icons large-block-grid-4 small-block-grid-4 large-push-2 large-8 small-12'>
				<?php while(have_rows('icons', 'options')): the_row(); ?>
					<?php $icon_type = get_sub_field('icon_type'); ?>
					<?php $icon_block = get_sub_field('icon_block'); ?>
					<?php $icon_hover = get_sub_field('icon_hover'); ?>
					<?php $text = urlencode(get_sub_field('share_text')); ?>
					<?php $hashtags = urlencode(get_sub_field('share_text')); ?>
					<li class='share-icon'>
				  	<?php
				  		switch ($icon_type) {
				  			case 'twitter':?>
									<a class="twitter" href="#twitter" onclick="popUp=window.open('https://twitter.com/share?url=<?php echo $url; ?>&amp;text=<?php echo $text; ?>&amp;hashtags=<?php echo $hashtags; ?>', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false">
										<img data-hover="<?php echo $icon_hover['url']; ?>" data-src="<?php echo $icon_block['url']; ?>" src="<?php echo $icon_block['url']; ?>">
									</a>
				  			<?php break;
				  			case 'google-plus':?>
									<a class="google-plus" href="#google-plus" onclick="popUp=window.open('https://plus.google.com/share?url=<?php the_permalink(); ?>', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false">
										<img data-hover="<?php echo $icon_hover['url']; ?>" data-src="<?php echo $icon_block['url']; ?>" src="<?php echo $icon_block['url']; ?>">
									</a>
				  			<?php break;
				  			case 'facebook':?>
									<a class="facebook" title="Share on Facebook" href="#fb" onclick="popUp=window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=Remedy&amp;p[summary]=<?php echo $text; ?>&amp;u=<?php echo $url; ?>&amp;p[images[0]=<?php echo $image; ?>', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false">
										<img data-hover="<?php echo $icon_hover['url']; ?>" data-src="<?php echo $icon_block['url']; ?>" src="<?php echo $icon_block['url']; ?>">
									</a>
				  			<?php break;
				  			case 'pinterest':?>
									<a class="pinterest pin-it-button" href="#pinterest" onclick="popUp=window.open('http://pinterest.com/pin/create/button/?url=<?php echo $url; ?>&amp;media=<?php echo $image; ?>&amp;description=<?php echo $text; ?>', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false">
										<img data-hover="<?php echo $icon_hover['url']; ?>" data-src="<?php echo $icon_block['url']; ?>" src="<?php echo $icon_block['url']; ?>">
									</a>
				  			<?php break;
				  		}
				  	?>
				  </li><!-- .share-icon -->
				<?php endwhile; ?>
			</ul><!-- .share-icons -->
		<?php endif; ?>
</div>
