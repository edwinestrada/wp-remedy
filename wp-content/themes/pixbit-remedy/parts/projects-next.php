<?php
/**
 * ACF Section - Next Block Section
 */
?>
<?php $next_image = get_field('next_image'); ?>
<?php $next_project = get_field('next_project'); ?>
<?php if($next_image): ?>
	<style type="text/css">
		.footer-section{
			background-image: url(<?php echo $next_image['url']; ?>);
		}
	</style>
	<div class="footer-section bg-img">
		<div class="footer-content">
			<a id="next-project" class="banner-link" href="<?php echo get_permalink($next_project->ID); ?>">Next Project</a>
		</div>
	</div>
<?php endif; ?>
