<?php
/**
 * ACF Section - Text Section
 */
?>
<?php $is_full_width = get_sub_field('is_full_width'); ?>
<?php $text_image = get_sub_field('text_image'); ?>
<?php if($text_image): ?>
<style type="text/css">
	#text-section-<?php echo $section_counter; ?>{
		background-image: url(<?php echo $text_image['url']; ?>);
	  background-position: center top;
	  -webkit-background-size: cover;
	  -moz-background-size: cover;
	  -o-background-size: cover;
	  background-size: cover;
	  position: relative;
	}
	#text-section-<?php echo $section_counter; ?> .row{
		padding-left: 0;
		padding-right: 0;
		background: none;
	}
</style>
<?php endif; ?>
<div id="text-section-<?php echo $section_counter; ?>" class="text-section<?php echo $is_full_width ? ' full-text-section' : ''; ?> bg-img">
	<div class="row">
		<div class="large-12 columns">

			<?php if( have_rows('texts') ): ?>
				<?php
					$long_texts = array();
					$text_names = array();
				?>
				<?php while( have_rows('texts') ): the_row(); 
					$long_text = get_sub_field('long_text');
					$text_name = get_sub_field('text_name');
					array_push($long_texts, $long_text);
					array_push($text_names, $text_name);
					?>
				<?php endwhile; ?>
			<?php endif; ?>

			<?php $separator = 'SEPARATOR'; ?>
			<p id="p-quote-rotator-<?php echo $section_counter; ?>" class="long-text quote-rotator">
			<?php for ($i=0; $i < count($long_texts); $i++): ?>
				<?php echo $long_texts[$i]; ?>
				<?php if($i !== (count($long_texts)-1)) echo $separator; ?>
			<?php endfor; ?>
			</p>
			<div class="hr"><hr /></div>
			<h2 id="h2-quote-rotator-<?php echo $section_counter; ?>" class="quote-rotator">
			<?php for ($j=0; $j < count($text_names); $j++): ?>
				<?php echo $text_names[$j]; ?>
				<?php if($j !== (count($text_names)-1)) echo $separator; ?>
			<?php endfor; ?>
			</h2>

			<script type="text/javascript">
				$('#p-quote-rotator-<?php echo $section_counter; ?>').css('min-height', '200px');
			  $('#p-quote-rotator-<?php echo $section_counter; ?>, #h2-quote-rotator-<?php echo $section_counter; ?>').textrotator({
			    animation: "dissolve", // You can pick the way it animates when rotating through words. Options are dissolve (default), fade, flip, flipUp, flipCube, flipCubeUp and spin.
			    separator: "SEPARATOR", // If you don't want commas to be the separator, you can define a new separator (|, &, * etc.) by yourself using this field.
			    speed: <?php echo get_sub_field('rotator_speed'); ?> // How many milliseconds until the next word show.
			  });
			</script>

		</div>
	</div>
<?php if(get_sub_field('enable_carrot')): ?>
	<span class="text-stem"></span>
<?php endif; ?>
</div>