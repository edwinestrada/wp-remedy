'use strict';
module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

		sass: {
			dist: {
				options: {
				// 	banner:
				// 	'/*\n Theme Name: <%= pkg.name %> Theme\n Description: <%= pkg.description %>\n Author: <%= pkg.author %>\n Author URI: http://thinkpixbit.com\n Version: <%= pkg.version %>\n Tags: <%= pkg.keywords %>\n */'
					style: 'compressed'//,
					// style: 'compact'//,
				},
				files: {
          // 'stylesheets/bootstrap.css' : 'bower_components/bootstrap-sass-official/vendor/assets/stylesheets/bootstrap.scss',
          // 'stylesheets/font-awesome.css' : 'bower_components/fontawesome/scss/font-awesome.scss',
          // 'stylesheets/bootstrap-blossom.css' : 'assets/sass/bootstrap-blossom.scss',
          // 'stylesheets/header.css' : 'assets/sass/header.scss',
          'style.css' : 'style.scss'//,
          // 'fonts/coming_soon.css' : 'fonts/coming_soon.scss',
          // 'fonts/tint.css' : 'fonts/tint.scss'//,
          // 'stylesheets/simpletextrotator.css' : 'assets/sass/simpletextrotator.scss'
				}
			}
		},
    concat: {
      js: {
        src: [
          'bower_components/sidr/jquery.sidr.min.js',
          'js/remedy.js'
        ],
        dest: 'build/js/app.js'
      }
      // ,css: {
      //   options: {
      //     banner: '/*\n Theme Name: <%= pkg.name %> Theme\n Description: <%= pkg.description %>\n Author: <%= pkg.author %>\n Author URI: http://thinkpixbit.com\n Version: <%= pkg.version %>\n Tags: <%= pkg.keywords %>\n */'
      //   },
      //   src: [
      //     // 'fonts/stylesheet.css',
      //     'fonts/tint.css'//,
      //   ],
      //   dest: 'fonts/coming_soon.css'
      // }
    },
    // uglify: {
    //   build: {
    //     files: {
    //       'public/build/js/modernizr.min.js' : 'tmp/modernizr.js',
    //       'public/build/js/app.min.js' : 'tmp/app.js'
    //     }
    //   }
    // },
    watch: {
      css: {
				files: '**/*.scss',
        // tasks: ['sass', 'concat']
				tasks: ['sass']
			},
      js: {
        files: [
          'js/remedy.js'//,
        ],
        tasks: [
          'concat'//, 'uglify'
        ]
      }//,
    }


  });

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-sass'); // npm install grunt-contrib-sass --save-dev
  grunt.loadNpmTasks('grunt-contrib-watch'); // npm install grunt-contrib-watch --save-dev
  grunt.loadNpmTasks('grunt-contrib-concat'); // npm install grunt-contrib-concat --save-dev
  grunt.loadNpmTasks('grunt-contrib-uglify'); // npm install grunt-contrib-uglify --save-dev
  // grunt.loadNpmTasks('grunt-contrib-cssmin'); // npm install grunt-contrib-cssmin --save-dev

  grunt.registerTask('default', [
    'concat'
    //'watch',
    // 'sass'
  ]);

};
